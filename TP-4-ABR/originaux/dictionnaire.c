/*
 * Interface Python - C du TP.
 * Ce fichier ne concerne que la dernière section du TP.
 * Il ne comporte aucun travail à faire.
 */

#include <Python.h>
#include <structmember.h>
#include <numpy/ndarraytypes.h>
#include <numpy/ndarrayobject.h>
#include "abr.h"

/*
 * Un struct Python_ABR est un objet Python qui encapsule un ABR
 */

struct Python_ABR
{
  PyObject_HEAD 
  struct abr* racine;
};

/*
 * Le destructeur + free. En C++ ce serait delete
 */

static void
Python_ABR_dealloc (struct Python_ABR *self)
{
  printf ("Appel au destructeur de Python_ABR\n");
  clear_abr (self->racine);
  Py_TYPE (self)->tp_free ((PyObject *) self);
}

/*
 * En C++, new = malloc + constructeur
 * En Python, le constructeur sera appelé séparément donc new = malloc
 */

static PyObject *
Python_ABR_new (PyTypeObject * type, PyObject * args, PyObject * kwds)
{
  struct Python_ABR *self;

  printf ("Appel à new Python_ABR\n");
  self = (struct Python_ABR *) type->tp_alloc (type, 0);
  return (PyObject *) self;
}

/*
 * Le constructeur par défaut. Initialise self au dictionnaire vide
 */

static int
Python_ABR_init (struct Python_ABR *self, PyObject * args, PyObject * kw)
{
  printf ("Appel au constructeur de Python_ABR\n");
  self->racine = (struct abr*)0;
  return 0;
}

/*
 * Remplit un ABR avec des valeurs (clé, satellite) 
 * Le paramètre est un ndarray numpy de dimension n x 2.
 * table [i][0] = clé, table[i][1] = satellite (pour tout i)
 */

static PyObject *
ajouter (struct Python_ABR *self, PyObject * args, PyObject * kw)
{
  static char *kwlist[] = { "table", NULL };
  PyArrayObject* table = (PyArrayObject*)-1;
  int n;

  if (!PyArg_ParseTupleAndKeywords (args, kw, "O", kwlist, &table))
    return NULL;

  n = PyArray_DIM (table, 0);
  for (int i = 0; i < n; i++)
    {   
        wstring clef;
        wstring satellite;
        PyObject* objet;

        objet = PyArray_GETITEM (table, PyArray_GETPTR2 (table, i, 0));
        PyUnicode_AsWideChar (objet, clef, MAXLEN);
    
        objet = PyArray_GETITEM (table, PyArray_GETPTR2 (table, i, 1));
        PyUnicode_AsWideChar (objet, satellite, MAXLEN);

        self->racine = ajout_abr (self->racine, clef, satellite);
    }

  Py_RETURN_NONE;
}

/*
 * Rechercher une clé dans un ABR. Retourne None si non trouvé
 */

static PyObject *
rechercher (struct Python_ABR *self, PyObject * args, PyObject * kw)
{
  static char *kwlist[] = { "cle", NULL };
  PyObject* key;
  wstring cle;
  wchar_t* satellite;

  if (!PyArg_ParseTupleAndKeywords (args, kw, "O", kwlist, &key))
    return NULL;

  PyUnicode_AsWideChar (key, cle, MAXLEN);
  satellite = recherche_abr (self->racine, cle);

  if (satellite)
    {
      PyObject* result;
      result = PyUnicode_FromWideChar (satellite, -1);
      Py_INCREF (result);
      return result;
    }
  else
      Py_RETURN_NONE;
}

/*
 * Retourne la hauteur de l'ABR
 */

static PyObject *
hauteur (struct Python_ABR *self)
{
    long h = hauteur_abr (self->racine);
    return PyLong_FromLong (h);
}

/*
 * Le tableau des méthodes
 */

static PyMethodDef Python_ABR_methods[] = {
  {"ajouter", (PyCFunction) ajouter, METH_VARARGS | METH_KEYWORDS, "ajouter"},
  {"rechercher", (PyCFunction) rechercher, 
                                METH_VARARGS | METH_KEYWORDS, "rechercher"},
  {"hauteur", (PyCFunction) hauteur, METH_NOARGS, "hauteur"},
  {NULL}                        /* Sentinel */
};

/*
 * Le type Python_ABR
 */

static PyTypeObject dictionnaire_Python_ABRType = {
  PyVarObject_HEAD_INIT (NULL, 0) "dictionnaire.Python_ABR",      /* tp_name */
  sizeof (struct Python_ABR),       /* tp_basicsize */
  0,                            /* tp_itemsize */
  (destructor) Python_ABR_dealloc,  /* tp_dealloc */
  0,                            /* tp_print */
  0,                            /* tp_getattr */
  0,                            /* tp_setattr */
  0,                            /* tp_reserved */
  0,                            /* tp_repr */
  0,                            /* tp_as_number */
  0,                            /* tp_as_sequence */
  0,                            /* tp_as_mapping */
  0,                            /* tp_hash  */
  0,                            /* tp_call */
  0,                            /* tp_str */
  0,                            /* tp_getattro */
  0,                            /* tp_setattro */
  0,                            /* tp_as_buffer */
  Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE,     /* tp_flags */
  "Python_ABR objet",               /* tp_doc */
  0,                            /* tp_traverse */
  0,                            /* tp_clear */
  0,                            /* tp_richcompare */
  0,                            /* tp_weaklistoffset */
  0,                            /* tp_iter */
  0,                            /* tp_iternext */
  Python_ABR_methods,               /* tp_methods */
  0,                            /* tp_members */
  0,                            /* tp_getset */
  0,                            /* tp_base */
  0,                            /* tp_dict */
  0,                            /* tp_descr_get */
  0,                            /* tp_descr_set */
  0,                            /* tp_dictoffset */
  (initproc) Python_ABR_init,       /* tp_init */
  0,                            /* tp_alloc */
  Python_ABR_new,                   /* tp_new */
};

/*
 * Le module dictionnaire auquel le type Python_ABR appartient
 */

static PyModuleDef dictionnairemodule = {
  PyModuleDef_HEAD_INIT,
  "dictionnaire",
  "Définit le type Python_ABR",
  -1,
  NULL, NULL, NULL, NULL, NULL
};

/*
 * Fonction qui initialise le module
 */

PyMODINIT_FUNC
PyInit_dictionnaire (void)
{
  PyObject *m;

  if (PyType_Ready (&dictionnaire_Python_ABRType) < 0)
    return NULL;

  m = PyModule_Create (&dictionnairemodule);
  if (m == NULL)
    return NULL;
/*
 * Pas très propre parce que dictionnaire_Python_ABRType est une structure
 * qui appartient à la zone des variables globales initialisées (data)
 * et pas au tas : l'incrémentation du compteur de référence a 
 * pour effet d'éviter un appel fautif au destructeur
 */
  Py_INCREF (&dictionnaire_Python_ABRType);
  PyModule_AddObject (m, "abr", (PyObject *) & dictionnaire_Python_ABRType);
  return m;
}
