Ne modifiez pas les originaux. 
Faites des copies dans le répertoire TP-1-allocation-dynamique et modifiez 
    les copies.

Ce répertoire contient 

un squelette de compte-rendu du TP 
----------------------------------
Deux formats (au choix) :

1. le format OpenDocument (Libreoffice, Word, ...) : compte-rendu-de-tp.odt
2. le format LaTeX : compte-rendu-de-tp.tex
    Voir compte-rendu-de-tp.pdf pour les détails

un Makefile incomplet
---------------------
Note : vous n'êtes pas obligés d'utiliser un Makefile

Pour le compléter, dans le répertoire courant, entrer la commande 

    gcc -MM *.c

Recopier les lignes obtenues en fin de Makefile
Ensuite :

    make
    make clean


