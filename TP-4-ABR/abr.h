
#include <stdlib.h>
#include <stdbool.h>
struct abr {
	struct abr* gauche;
	struct abr* droite;
	int valeur;
};

#define NIL ((void *)0)

extern struct abr* ajouter_abr(int, struct abr*);
extern int hauteur_abr(struct abr*);
extern void afficher_abr(struct abr*);
extern int nombre_noeuds_abr(struct abr*);
extern bool rechercher_abr(struct abr*,int);
extern void clear_abr(struct abr*);
extern void imp_dot(struct abr* A);


