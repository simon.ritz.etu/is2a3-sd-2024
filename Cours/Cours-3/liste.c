// liste.c

#include <stdio.h>
#include <stdlib.h>
#include "liste.h"

// Constructeur. Initialise la liste à vide
void init_liste (struct liste* L)
{
  L->nbelem = 0;
  L->tete = NIL;
}

// Ajoute d (mode D) en tête de L (mode DR)
void ajout_en_tete_liste (struct liste* L, double d)
{
  struct maillon* M;
// 1.
  M = (struct maillon*)malloc (sizeof (struct maillon));
// 2.
  M->valeur = d;
// 3.
  M->suivant = L->tete;
// 4.
  L->tete = M;
// 5.
  L->nbelem += 1;
}

void imprimer_liste (struct liste L)
{
  struct maillon* M;

  printf ("[");
  M = L.tete;
  for (int i = 0; i < L.nbelem; i++)
    {
      if (i > 0)
        printf (", ");
      printf ("%lf", M->valeur);
      M = M->suivant;
    }
  printf ("]\n");
}

// Destructeur
void clear_liste (struct liste* L)
{
}

