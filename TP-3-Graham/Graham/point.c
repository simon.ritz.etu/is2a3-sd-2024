#include "point.h"

extern void init_point(struct point* P,double d1, double d2, char c){
	P->x = d1;
	P->y=d2;
	P->ident = c;
}

extern int compare_points(const void*p0, const void *q0){
	struct point *p = (struct point *) p0;
	struct point *q = (struct point *) q0;
	
	if (p->x * q->y - p->y * q->x >0)
		return -1;
	else if (p->x * q->y - p->y * q->x == 0)
		return 0;
	else 
		return 1;
}

extern bool tourne_a_gauche (struct point *A, struct point *B, struct point *C){
	return (B->x - A->x)*(C->y - A->y) - (B->y - A->y)*(C->x - A->x) > 0;
}
	
