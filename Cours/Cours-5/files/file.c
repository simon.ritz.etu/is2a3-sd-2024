#include <assert.h>
#include "file.h"

void init_file (struct file* F)
{
  F->we = 4; // on suppose N >= 6
  F->re = 5;
}

void clear_file (struct file* F)
{
}

// Enfile c dans F
void enfiler (struct file* F, char c)
{
  assert (! est_pleine_file (F));
  F->we = (F->we + 1) % N;
  F->T [F->we] = c;
}

bool est_vide_file (struct file* F)
{
  return (F->we + 1) % N == F-> re;
}

bool est_pleine_file (struct file* F)
{
  return (F->we + 2) % N == F-> re;
}

// Défile un caractère et le retourne
char defiler (struct file* F)
{
  char c;

  assert (! est_vide_file (F));
  c = F->T [F->re];
  F->re = (F->re + 1) % N;
  return c;
}

