// abr.c

#include <stdio.h>
#include <stdlib.h>
#include "abr.h"

static struct abr* new_feuille (double d)
{
  struct abr* F;
  F = (struct abr*)malloc (sizeof (struct abr));
  F->gauche = NIL;
  F->valeur = d;
  F->droite = NIL;
  return F;
}

// Retourne l'ABR obtenu en ajoutant d à A0

struct abr* ajouter_abr (struct abr* A0, double d)
{
  if (A0 == NIL)
    return new_feuille (d);
  else
    {
      struct abr* A = A0;
      struct abr* B = NIL; // inutile

      while (A != NIL)
        {
          B = A;
          if (A->valeur < d)
            A = A->droite;
          else
            A = A->gauche;
        }
/*
 * Ici, A vaut NIL et B contient le struct abr* à modifier
 */
      if (B->valeur < d)
        B->droite = new_feuille (d);
      else
        B->gauche = new_feuille (d);
/*
 * On retourne l'ABR obtenu après ajout
 */
      return A0;
    }
}

bool rechercher_abr (struct abr* A0, double d)
{
  struct abr* A = A0;
  bool found = false;

  while (A != NIL && !found)
    {
      if (A->valeur == d)
        found = true;
      else if (A->valeur < d)
        A = A->droite;
      else
        A = A->gauche;
    }

  return found;
}

static void imprimer2_abr (struct abr* A)
{
  if (A != NIL)
    {
      imprimer2_abr (A->gauche);
      printf ("%lf ", A->valeur);
      imprimer2_abr (A->droite);
    }
}

void imprimer_abr (struct abr* A)
{
  printf ("A = ");
  imprimer2_abr (A);
  printf ("\n");
}

void clear_abr (struct abr* A0)
{
}

