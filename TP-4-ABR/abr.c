#include <stdio.h>
#include <assert.h>

#include "abr.h"
#define MIN(i, j) (((i) < (j)) ? (i) : (j))
#define MAX(i, j) (((i) > (j)) ? (i) : (j))


static struct abr* new_feuille(int n){
	struct abr*F;
	F = (struct abr*) malloc(sizeof(struct abr));
	F->gauche = NIL;
	F->droite =NIL;
	F->valeur = n;
	return F;
}

struct abr* ajouter_abr(int n, struct abr* A0){
	if (A0 == NIL)
		return new_feuille(n);
	else{
		struct abr* A;
		struct abr* B;
		B=NIL;
		A=A0;
		while(A!=NIL){
			B=A;
			if (A->valeur < n)
				A=A->droite;
			else
				A=A->gauche;
		}
		if (B->valeur<n){
			B->droite = new_feuille(n);}
		else
			B->gauche = new_feuille(n);
		return A0;
	}
}

int hauteur_abr(struct abr* A0){
	if (A0 == NIL)
		return -1;
	else{
		return MAX(hauteur_abr(A0->gauche),hauteur_abr(A0->droite)) + 1;
	}
}

void afficher_abr2(struct abr* A){
	if (A!= NIL){
		printf("%d", A->valeur);
		afficher_abr2(A->gauche);
		afficher_abr2(A->droite);
	}
}
void afficher_abr(struct abr* A){
	printf("votre ABR : ");
	afficher_abr2(A);
	printf("\n");
}
	

int nombre_noeuds_abr(struct abr* A){
	if (A==NIL){
		return 0;
	}
	else{
		return nombre_noeuds_abr(A->gauche) + nombre_noeuds_abr(A->droite) + 1;
	}
}

bool rechercher_abr(struct abr* A,int n){
	if (A == NIL)
		return false;
	else{
		if (A->valeur == n)
			return true;
		else if(A->valeur < n)
			rechercher_abr(A->droite,n);
		else
			rechercher_abr(A->gauche,n);
	}
}

void clear_abr(struct abr* A){
	if(A!=NIL){
		clear_abr(A->gauche);
		clear_abr(A->droite);
		free(A);
	}
	else
		free(A);
}

bool est_feuille(struct abr* A){
	return (A!=NIL) && (A->gauche == NIL && A->droite ==NIL);
}

static void imp_dot2(FILE* f,struct abr* A){
	if(A->gauche != NIL){
		fprintf(f,"%d->%d[label =\"gauche\"];\n",A->valeur, A->gauche->valeur);
		imp_dot2(f,A->gauche);
	}
	if(A->droite != NIL) {
		fprintf(f,"%d->%d[label =\"droite\"];\n",A->valeur, A->droite->valeur);
		imp_dot2(f,A->droite);
	}
}

void imp_dot(struct abr* A){
	FILE* f;
	f=fopen("abr.dot","w");
	assert(f!=(FILE*)0);
	fprintf(f, "digraph G {\n");
	if(A==NIL){
		fprintf(f, "NIL;\n");
	} else if(est_feuille(A)){
		fprintf(f, "%d; \n", A->valeur);
	}else{
		imp_dot2(f,A);
	}
	fprintf(f, "}\n");
	fclose(f);
	system("dot -Tpdf abr.dot -Grankdir=LR -o abr.pdf");
	system("evince abr.pdf");
}










