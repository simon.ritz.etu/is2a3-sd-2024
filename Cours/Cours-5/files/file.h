// file.h

#include <stdbool.h>

#define N 6

struct file {
    char T [N];
    int re;
    int we;
};

/*
 * re = l'indice du prochain élément à être défilé
 * we = l'indice du dernier élément enfilé
 * la file vide se reconnaît à (we + 1) % N == re
 * la file pleine à (we + 2) % N == re
 */

extern void init_file (struct file*);

extern void clear_file (struct file*);

// Emfile c dans F
extern void enfiler (struct file* F, char c);

extern bool est_vide_file (struct file*);

extern bool est_pleine_file (struct file*);

// Défile un caractère et le retourne
extern char defiler (struct file* F);

