#include "liste_char.h"
#include <stdio.h>
#include <stdlib.h>

void init_liste_char(struct liste_char *liste) {
    liste->tete = NULL;
    liste->length = 0;
}

void ajout_en_queue_liste_char(struct liste_char *liste, char c) {
    struct maillon_char *nouveau = malloc(sizeof(struct maillon_char));
    if (nouveau == NULL) {
        return;
    }
    nouveau->c = c;
    nouveau->next = NULL;
    
    if (liste->tete == NULL) {
        liste->tete = nouveau;
    } else {
        struct maillon_char *temp = liste->tete;
        while (temp->next != NULL) {
            temp = temp->next;
        }
        temp->next = nouveau;
    }
    liste->length++;
}

void print_liste_char(const struct liste_char *liste) {
    struct maillon_char *temp = liste->tete;
    while (temp != NULL) {
        printf("%c", temp->c);
        temp = temp->next;
    }
    printf("\n");
}

void clear_liste_char(struct liste_char *liste) {
    struct maillon_char *temp = liste->tete;
    while (temp != NULL) {
        struct maillon_char *suivant = temp->next;
        free(temp);
        temp = suivant;
    }
    liste->tete = NULL;
    liste->length = 0;
}
