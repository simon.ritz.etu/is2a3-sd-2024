// main.c

#include <stdio.h>
#include "file.h"

int main ()
{
  char data [] = { 'e', 'Z', 'u', '9' };
  int n = sizeof(data)/sizeof(data[0]);
  struct file F;

  init_file (&F);

  for (int i = 0; i < n; i++)
    {
      printf ("on enfile '%c'\n", data[i]);
      enfiler (&F, data[i]);
    }

  while (! est_vide_file (&F))
    {
      char c;
      c = defiler (&F);
      printf ("on vient de defiler '%c'\n", c);
    }

  clear_file (&F);
  return 0;
}
