// rationnel.c

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "rationnel.h"

/* retourne le pgcd de a0 et b0 */

static int
Euclide (int a0, int b0)
{
  int a, b, t;

  if (a0 > 0)
    a = a0;
  else
    a = -a0;
  if (b0 > 0)
    b = b0;
  else
    b = -b0;
  while (b != 0)
    {
      t = a % b;		/* le reste de la division de a par b */
      a = b;
      b = t;
    }
  return a;
}

// Ceci n'est pas un constructeur.
// Affecte p0/q0 à A
void set_rationnel (struct rationnel *A, int p0, int q0)
{
  int g, p, q;

  assert (q0 != 0);		// arrêt brutal du processus si q == 0

// Pas de malloc: il a déjà été fait par un constructeur

  g = Euclide (p0, q0);
  p = p0 / g;
  q = q0 / g;
  if (q > 0)
    {
      A->data[0] = p;
      A->data[1] = q;
    }
  else
    {
      A->data[0] = -p;
      A->data[1] = -q;
    }
}

// Constructeur. Initialise A à p0/q0. A mode R.
void
init_rationnel (struct rationnel *A, int p0, int q0)
{
  A->data = (int*)malloc (2 * sizeof (int));
// Note: les deux affectations ci-dessous sont inutiles
//       mais respectent les spécifications des rationnels
  A->data[0] = 0;
  A->data[1] = 1;

  set_rationnel (A, p0, q0);
}

// Affecte B + C à A. A en mode R et B, C en mode D

void
add_rationnel (struct rationnel *A, struct rationnel *B, struct rationnel *C)
{
  int p, q;

  p = B->data[0] * C->data[1] + C->data[0] * B->data[1];
  q = B->data[1] * C->data[1];
  set_rationnel (A, p, q);
}

void
print_rationnel (struct rationnel *A)
{
  if (A->data[1] == 1)
    printf ("%d\n", A->data[0]);
  else
    printf ("%d/%d\n", A->data[0], A->data[1]);
}

// Le destructeur
void
clear_rationnel (struct rationnel *A)
{
  free (A->data);
}
