#include"liste_char.h"

struct chaine{
	struct liste_char L;
};


void init_chaine(struct chaine *S);
void ajout_en_queue_chaine(struct chaine *S, char c);
void print_chaine(struct chaine *S);
void clear_chaine(struct chaine *S);
