/* hachage_simple.h */
#include "liste_double.h"
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

struct alveole {
	struct liste_double L;
};

#define N 10
struct table {
	struct alveole tab [N];
};

extern void init_table (struct table*);
extern void clear_table (struct table*);
extern void enregistrer_table (struct table*, double);
extern bool rechercher_table (struct table*, double);
extern void imprimer_table (struct table*);
