# Résolution de la récurrence de l'algorithme de Karatsuba avec Python

from sympy import *

print ("Résolution de la récurrence de l'algorithme de Karatsuba")

f = Function('f')
g = Function('g')
k, n = symbols('k n', integer=True)

# L'équation de récurrence initiale
eq_f = f(n) - ( 3*f(n/2) + 4*n - 3 )

print ('équation de récurrence initiale =', eq_f)

# On suppose n = 2**k et donc k = log2(n) = log(n)/log(2)
# On pose g(k) = f(n) et donc g(k-1) = f(n/2)
# On remplace (f(n), f(n/2), n) par (g(k), g(k-1), 2**k)

eq_k = eq_f.subs([(f(n),g(k)), (f(n/2),g(k-1))]).subs ([(n,2**k)])

print ('équation de récurrence après changement de variable =', eq_k)

# La solution 

solk = rsolve (eq_k, g(k), {g(0):2})

print ('solution exprimée avec k =', solk)

# On remplace k par log2(n)

soln = solk.subs ([(k,log(n)/log(2))])

print ('solution exprimée avec n =', soln)

# On remplace 3**log2(n) par n**log2(3)

soln = soln.subs ([(3**(log(n)/log(2)),n**(log(3)/log(2)))])

print ('solution après réécriture du log =', soln)

