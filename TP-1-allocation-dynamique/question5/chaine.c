#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include "chaine.h"

void init_chaine(struct chaine *S) {
    init_liste_char(&S->L); 
}


void ajout_en_queue_chaine(struct chaine *S, char c) {
     ajout_en_queue_liste_char(&S->L , c);
}

void print_chaine(struct chaine *S) {
    print_liste_char(&S->L);
}

void clear_chaine(struct chaine *S) {
    clear_liste_char(&S->L);
}
