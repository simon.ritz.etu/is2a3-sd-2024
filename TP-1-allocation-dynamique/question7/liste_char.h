struct maillon_char{
	char c;
	struct maillon_char* next;
};

struct liste_char{
	struct maillon_char* tete;
	int length;
};

void init_liste_char(struct liste_char *liste);
void ajout_en_queue_liste_char(struct liste_char *liste, char c);
void print_liste_char(struct liste_char *liste);
void clear_liste_char(struct liste_char *liste);
