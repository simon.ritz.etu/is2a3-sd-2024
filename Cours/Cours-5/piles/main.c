// main.c

#include <stdio.h>
#include "pile.h"

int main ()
{
  char data [] = { 'e', 'Z', 'u', '9' };
  int n = sizeof(data)/sizeof(data[0]);
  struct pile P;

  init_pile (&P);

  for (int i = 0; i < n; i++)
    {
      printf ("on empile '%c'\n", data[i]);
      empiler (&P, data[i]);
    }

  while (! est_vide_pile (&P))
    {
      char c;
      c = depiler (&P);
      printf ("on vient de depiler '%c'\n", c);
    }

  clear_pile (&P);
  return 0;
}
