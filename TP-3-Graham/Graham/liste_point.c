#include "liste_point.h"
#include <stddef.h>
#include <stdlib.h>

extern void init_liste_point(struct liste_point *L){
	L->tete = NULL;
	L->length = 0;
}

extern void clear_liste_point(struct liste_point *L){
    struct maillon_point *temp = L->tete;
    while (temp != NULL) {
        struct maillon_point *suivant = temp->next;
        free(temp);
        temp = suivant;
    }
    L->tete = NULL;
    L->length = 0;
}


extern void ajouter_en_tete_liste_point(struct liste_point *L, struct point pt){
	struct maillon_point *nouveau = malloc(sizeof(struct maillon_point));
	nouveau->P = pt;
	nouveau->next = L->tete;
	L->tete = nouveau;
	L->length +=1;
}

extern void extraire_tete_liste_point(struct point* pt,struct liste_point *L){
	struct maillon_point*tete;
	if(L->length ==0){
		exit(1);}
	tete = L->tete;
	*pt = tete->P;
	L->tete = tete->next;
	L->length -=1;
	free(tete);
}


