/* Graham.c */

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "liste_point.h"

#define N 10
#define SCENARIO 73738

/*
 * Trie le tableau T (mode D/R) par angle polaire croissant
 * Retourne true si les points ont des angles polaires distincts deux-à-deux
 * Retourne false sinon
 * n (mode D) = le nombre de points
 */

bool trie_points (int n, struct point *T)
{
  qsort (T, n, sizeof (struct point), &compare_points);
  for (int i = 0; i < n - 1; i++)
    if (compare_points (&T[i], &T[i + 1]) == 0)
      return false;
  return true;
}

/*
 * Algorithme de Graham
 *
 * Les paramètres T et n sont en mode Donnée.
 * Le tableau T contient les points dont on cherche l'enveloppe.
 *    Il est trié par angle polaire croissant.
 * L'entier n contient le nombre de points présents dans T.
 *
 * Les paramètres E et m sont en mode Résultat.
 * Le tableau E reçoit les points de l'enveloppe convexe.
 *    Il est suffisamment grand.
 * L'entier m reçoit le nombre de points de l'enveloppe.
 */

void Graham (int *m, struct point *E, int n, struct point *T){
	struct liste_point L;
	struct point * = poub;

	quick_sort(T,n,sizeof(struct point),&compare_points);
	init_liste_point(&L);
	ajouter_en_tete_liste_point(&L, T[0]);
	ajouter_en_tete_liste_point(&L, T[1]);
	int i = 2;
	while (i<n){
		struct point *cour = L.tete->P;
		struct point *prec = L.tete->next->P;
		if (tourne_a_gauche(prec,cour,&T[i])){
			ajouter_en_tete_liste_point(&L, T[i]);
			i+=1;
		}
		else
			extraire_tete_liste_point(poub,&L);
	}
	m=0;
	while(L->tete != NULL){
		extraire_tete_liste_point(&E[m],&L);
		m+=1;
	}
	
			
		

}

int main ()
{
  struct point T[N], E[N + 1];
  FILE *f;
  int i, m;

  srand48 (SCENARIO);
/* On crée N points qu'on enregistre dans "points.dat" (A est en (0,0)). */
  init_point (&T[0], 0, 0, 'A');
  for (i = 1; i < N; i++)
    {
      double x, y;
      x = drand48 ();
      y = drand48 ();
      init_point (&T[i], x, y, 'A' + i);
    }
  f = fopen ("points.dat", "w");
  assert (f != NULL);
  for (i = 0; i < N; i++)
    fprintf (f, "%f %f %c\n", T[i].x, T[i].y, T[i].ident);
  fclose (f);
/* Vérification */
  if (!trie_points (N - 1, T + 1))
    {
      fprintf (stderr, "alignement de points non traité\n");
      exit (1);
    }
/* Affichage pour debugger */
  printf ("par angle polaire croissant :");
  for (i = 0; i < N; i++)
    printf (" %c", T[i].ident);
  printf ("\n");
/* Application de l'algo de Graham. Résultat dans m et E */
  Graham (&m, E, N, T);
/* Enregistrement des points de E dans "enveloppe.dat" */
  f = fopen ("enveloppe.dat", "w");
  assert (f != NULL);
  for (i = 0; i < m; i++)
    fprintf (f, "%f %f\n", E[i].x, E[i].y);
  fclose (f);
  return 0;
}
