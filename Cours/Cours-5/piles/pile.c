#include <assert.h>
#include "pile.h"

void init_pile (struct pile* P)
{
  P->sp = -1;
}

void clear_pile (struct pile* P)
{
}

// Empile c dans P
void empiler (struct pile* P, char c)
{
  assert (P->sp < N-1);
/*
 * Ou bien:
 *
  if (P->sp >= N-1)
    {
      fprintf (stderr, "erreur: la pile est plein\n");
      exit (1);
    }
 */
  P->sp += 1;
  P->T [P->sp] = c;
/*
 * Ou bien:
  P->T [++P->sp] = c;
 */
}

bool est_vide_pile (struct pile* P)
{
  return P->sp == -1;
}

// Dépile un caractère et le retourne
char depiler (struct pile* P)
{
  char c;

  assert (! est_vide_pile (P));
  c = P->T [P->sp];
  P->sp -= 1;
  return c;
/*
 * Ou bien:
 
  return P->T [P->sp--];
 */
}

