Le programme tirage_loto.c contient des erreurs.
L'exercice (décrit dans le support de cours) consiste à les trouver 
    avec gdb.

Attention à ne pas modifier les fichiers présents dans ce répertoire
sous peine de créer des conflits. Si vous voulez les modifier, faites
des copies et modifiez les copies

