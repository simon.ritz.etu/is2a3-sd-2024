// liste.h

struct maillon {
   double valeur;
   struct maillon* suivant;
};

#define NIL (struct maillon*)0

struct liste {
    struct maillon* tete;
    int nbelem;
};

/*
 * Le type struct liste implante des listes chaînées
 * Le champ nbelem contient le nombre d'éléments 
 * Le champ tete contient l'adresse du premier maillon
 * Les maillons sont alloués dynamiquement
 * Le champ suivant du dernier maillon vaut NIL
 * La liste vide est codée par (tete, nbelem) = (NIL, 0)
 */

// Constructeur. Initialise la liste à vide
extern void init_liste (struct liste*);

// Ajoute d (mode D) en tête de L (mode DR)
extern void ajout_en_tete_liste (struct liste* L, double d);

extern void imprimer_liste (struct liste);

// Destructeur
extern void clear_liste (struct liste*);

