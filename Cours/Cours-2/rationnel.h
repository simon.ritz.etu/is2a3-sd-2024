// rationnel.h

// 1. Déclaration du type
struct rationnel
{
  int* data;
};

// 2. Spécifications du type

/*
 * Le type struct rationnel implante des nombres rationnels
 * Le champ data point sur un tableau de 2 entiers alloués dans le tas (dynamiquement)
 * data[0] = numer
 * data[1] = denom
 * Le dénominateur est strictement positif
 * Les fractions sont réduites
 * Le signe est porté par le numérateur 
 */

// 3. Les prototypes des fonctions exportées

// Constructeur. Initialise A à p/q. A mode R.
extern void init_rationnel (struct rationnel *A, int p, int q);

// Affecte B + C à A. A en mode R et B, C en mode D
extern void add_rationnel (struct rationnel *A,
			   struct rationnel *B, struct rationnel *C);

extern void print_rationnel (struct rationnel *);

// Le destructeur
extern void clear_rationnel (struct rationnel *);
