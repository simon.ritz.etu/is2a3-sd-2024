// main.c

#include "rationnel.h"

// Tout l'art consiste à utiliser le type struct rationnel sans
// jamais avouer qu'on connaît l'implantation du type

int
main ()
{
  struct rationnel A, B;

  init_rationnel (&A, 3, 4);	// A = 3/4
  init_rationnel (&B, 14, -2);	// B = -7 = 14/(-2)
  add_rationnel (&A, &A, &B);	// A = A + B
  print_rationnel (&A);
  clear_rationnel (&A);		// destructeur
  clear_rationnel (&B);
  return 0;
}
