#include"hachage_simple.h"
#include<stdbool.h>
#include <math.h>

void init_table (struct table* T) {
	for(int i=0;i<N;i++){
		init_liste_double(&(T->tab[i].L));
	}
}

void clear_table (struct table* T){
	for(int i=0;i<N;i++){
		clear_liste_double(&(T->tab[i].L));
	}
}

void enregistrer_table (struct table* T, double d){
	ajouter_en_tete_liste_double(&(T->tab[(int)floor(d) % N].L), d);
}

bool rechercher_table (struct table* T, double d){
	return rechercher_liste_double(&(T->tab[(int)floor(d) % N].L), d);
}

void imprimer_table (struct table* T){
	for(int i=0;i<N;i++){
		imprimer_liste_double(&(T->tab[i].L));
		printf("\n");
	}
}




