#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>

int main(){
	int c;
	int i = 0;
	char *tab;
	c = getchar();
	tab = (char *) malloc(64 * sizeof(char));
	while (!isspace(c)){
		tab[i] = c;
		i = i+1;
		c = getchar();
	}
	for (int j=0;j<i ; j++){
		putchar(tab[j]);
	}
	putchar('\n');
	free(tab);
	return 0;
}
