// rationnel.c

#include <stdio.h>
#include <assert.h>
#include "rationnel.h"

/* retourne le pgcd de a0 et b0 */

static int
Euclide (int a0, int b0)
{
  int a, b, t;

  if (a0 > 0)
    a = a0;
  else
    a = -a0;
  if (b0 > 0)
    b = b0;
  else
    b = -b0;
  while (b != 0)
    {
      t = a % b;		/* le reste de la division de a par b */
      a = b;
      b = t;
    }
  return a;
}

// Constructeur. Initialise A à p/q. A mode R.
void
init_rationnel (struct rationnel *A, int p0, int q0)
{
  int g, p, q;

  assert (q0 != 0);		// arrêt brutal du processus si q == 0

  g = Euclide (p0, q0);
  p = p0 / g;
  q = q0 / g;
  if (q > 0)
    {
      A->numer = p;
      A->denom = q;
    }
  else
    {
      A->numer = -p;
      A->denom = -q;
    }
}

// Affecte B + C à A. A en mode R et B, C en mode D
// Rq. Il y a une maladresse dans cette fonction qui pourrait conduire
//     à un bug

void
add_rationnel (struct rationnel *A, struct rationnel *B, struct rationnel *C)
{
  int p, q;

  p = B->numer * C->denom + C->numer * B->denom;
  q = B->denom * C->denom;
  init_rationnel (A, p, q);
}

void
print_rationnel (struct rationnel *A)
{
  if (A->denom == 1)
    printf ("%d\n", A->numer);
  else
    printf ("%d/%d\n", A->numer, A->denom);
}

// Le destructeur
void
clear_rationnel (struct rationnel *A)
{
}
