// main.c

#include "liste.h"

int main ()
{
  struct liste L0;

  init_liste (&L0);                 // L0 = liste vide
  ajout_en_tete_liste (&L0, 108);   // L0 = [108]
  ajout_en_tete_liste (&L0, 42);    // L0 = [42, 108]
  ajout_en_tete_liste (&L0, 17);    // L0 = [17, 42, 108]
  imprimer_liste (L0);
  clear_liste (&L0);                // les trois maillons sont libérés
  return 0;
}
