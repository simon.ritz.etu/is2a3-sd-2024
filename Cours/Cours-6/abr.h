// abr.h

#include <stdbool.h>

struct abr {
    struct abr* gauche;
    double valeur;
    struct abr* droite;
};

#define NIL (struct abr*)0

extern struct abr* ajouter_abr (struct abr*, double);

extern bool rechercher_abr (struct abr*, double);

extern void clear_abr (struct abr*);

extern void imprimer_abr (struct abr*);

