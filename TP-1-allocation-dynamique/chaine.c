#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include "chaine.h"

void init_chaine(struct chaine *S) {
    S->capacity = 64;
    S->tab = (char *)malloc(S->capacity * sizeof(char));
    if (S->tab == NULL) {
        fprintf(stderr, "Erreur d'allocation mémoire\n");
        exit(1);
    }
    S->length = 0; 
}


void ajout_en_queue_chaine(struct chaine *S, char c) {
    if (S->length == S->capacity) {
        S->capacity += 64; 
        S->tab = (char *)realloc(S->tab, S->capacity * sizeof(char));
        if (S->tab == NULL) {
            fprintf(stderr, "Erreur de réallocation mémoire\n");
            exit(1);
        }
    }

    S->tab[S->length] = c;
    S->length += 1;
}

void print_chaine(struct chaine *S) {
    for (int i = 0; i < S->length; i++) {
        putchar(S->tab[i]);
    }
    putchar('\n');
}

void clear_chaine(struct chaine *S) {
    free(S->tab); 
    S->tab = NULL; 
    S->length = 0;
    S->capacity = 0; 
}

