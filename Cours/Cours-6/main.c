// main.c

#include <stdio.h>
#include "abr.h"

int main ()
{
  double data [] = { 17, 23, 35, 41, 6, 9, 21 };
  int N = sizeof (data) / sizeof (double);
  struct abr* A0;

  A0 = NIL;
  for (int i = 0; i < N; i++)
    {
      A0 = ajouter_abr (A0, data[i]);
      imprimer_abr (A0);
    }
  if (rechercher_abr (A0, 35))
    printf ("35 est dans l'ABR\n");
  else
    printf ("35 n'est pas dans l'ABR\n");
  clear_abr (A0);
  return 0;
}

