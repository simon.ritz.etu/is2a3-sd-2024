# Suppose que les fichiers 'Karatsuba.stats' et 'Elementaire.stats'
# sont disponibles

# Commande : python3 graphique.py

import numpy as np
import matplotlib.pyplot as plt
import math
from scipy.optimize import curve_fit

def f(n):
    return (17/2) * n**(math.log2(3)) -8*n +3/2

def F(n):
    return f(2*n)

def f1(n,a):
    return a * n**(math.log2(3)) 



K = np.loadtxt ('Karatsuba.stats')
E = np.loadtxt ('Elementaire.stats')
popt,pcov = curve_fit(f1,K[:,0],K[:,1])

a_optimal = popt[0]

n_values = np.linspace(min(K[:,0]), max(K[:,0]), 100)
plt.plot(n_values, f1(n_values, a_optimal), label='Ajustement Karatsuba', color='green')


plt.plot (K[:,0], K[:,1], label='Karatsuba')

plt.plot(K[:,0],[f(n) for n in K[:,0]],label='f')
plt.plot(K[:,0],[F(n) for n in K[:,0]],label='F')
plt.legend ()
plt.show ()

for i in range(599):
    if f(i) == K[i,1]:
        print(i)

