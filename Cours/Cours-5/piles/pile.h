// pile.h

#include <stdbool.h>

#define N 10

struct pile {
    char T [N];
    int sp;
};

/*
 * Les éléments sont empilés à partir de l'indice 0 dans T
 * sp = l'indice du dernier élément empilé
 * La pile vide est codée par sp = -1
 */

extern void init_pile (struct pile*);

extern void clear_pile (struct pile*);

// Empile c dans P
extern void empiler (struct pile* P, char c);

extern bool est_vide_pile (struct pile*);

// Dépile un caractère et le retourne
extern char depiler (struct pile* P);

